﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class sizeBackground : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	void Update ()
	{
		Vector2 rect = GetComponent<RectTransform> ().sizeDelta; 
		rect.y = GetComponentInChildren<Text> ().GetComponent<RectTransform> ().rect.height;
		GetComponent<RectTransform> ().sizeDelta = rect;

		//GetComponent<RectTransform>().rect.height = GetComponentInChildren<Text> ().GetComponent<RectTransform> ().rect.height;
	}
}
