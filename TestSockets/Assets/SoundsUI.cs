﻿using UnityEngine;
using System.Collections;

public class SoundsUI : MonoBehaviour
{
    public AudioClip[] _selectSounds;
    public AudioClip[] _cancelSounds;

    AudioSource _audio;

    void Start()
    {
        _audio = GetComponent<AudioSource>();
    }

    public void Select()
    {
        _audio.Stop();
        _audio.clip = _selectSounds[Random.Range(0, _selectSounds.Length)];
        _audio.Play();
    }

    public void Cancel()
    {
        _audio.Stop();
        _audio.clip = _cancelSounds[Random.Range(0, _cancelSounds.Length)];
        _audio.Play();
    }
}
