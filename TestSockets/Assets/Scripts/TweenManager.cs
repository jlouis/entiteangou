﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
 

public class TweenManager : MonoBehaviour
{
	public float _inValue = 900;
	public float _outValue = 2700;
	public float _time = 1.0f;

	List<GameObject> _animated = new List<GameObject>();

	RectTransform _canvas;

	void Start()
	{
		_canvas = GameObject.Find ("Canvas").GetComponent<RectTransform> ();
	}

	bool hasTween(GameObject obj)
	{
		return (_animated.IndexOf (obj) != -1);
	}

	void animationEnd()
	{
		_animated.RemoveAt (_animated.Count - 1);
	}

	public void GoIn(GameObject obj)
	{
		if (!hasTween (obj))
		{
			_animated.Add (obj);
			obj.transform.DOMoveX (0/*_canvas.rect.width / 2*/ * _canvas.localScale.x, _time).OnComplete(animationEnd);
			// 0 for Camera, + 0.5 * _canvas.rect.width for overlay
		}
	}

	public void GoOut(GameObject obj)
	{
		if (!hasTween (obj))
		{
			_animated.Add (obj);
			obj.transform.DOMoveX (_canvas.rect.width * 1.0f * _canvas.localScale.x, _time).OnComplete(animationEnd);
		}
	}
}
