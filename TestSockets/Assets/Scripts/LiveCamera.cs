﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public enum PhotoFormat
{
    PhotoJPG,
    PhotoPNG
}

public class LiveCamera : MonoBehaviour
{
    Texture2D _lastPhoto = null;
    public Image _lastPhotoViewer = null;

    public AppClient _client = null;
    public CollectionManager _collection = null;

	public string _folder = "";
    public PhotoFormat _format = PhotoFormat.PhotoJPG;

    WebCamDevice _webcam = new WebCamDevice();
	WebCamTexture _webcamTexture = null;
    string _lastPhotoPath = "";

	void Start ()
	{
		WebCamDevice[] devices = WebCamTexture.devices;
		for(var i = 0 ; i < devices.Length ; i++)
			Debug.Log(devices[i].name);

		if (devices.Length > 0)
			_webcam = devices [0];

		_webcamTexture = new WebCamTexture (_webcam.name);
		if (GetComponent<Renderer> ())
			GetComponent<Renderer> ().material.mainTexture = _webcamTexture;
		if (GetComponent<Image> ())
			GetComponent<Image> ().material.mainTexture = _webcamTexture;
		_webcamTexture.Play ();
	}

    string getFilename(int id)
    {
		string path = Application.persistentDataPath + _folder + "/EntPhoto" + id.ToString();
        if (_format == PhotoFormat.PhotoJPG)
            path += ".jpg";
        else
            path += ".png";

        return path;
    }

    public string GetLastPhotoPath()
    {
        return _lastPhotoPath;
    }

    public void CreateFileFromLastPhoto()
    {
        // Create the file
        int idFile = 0;
        while (File.Exists(getFilename(idFile)))
            idFile++;

		string folder = Application.persistentDataPath + _folder;
		if (!System.IO.Directory.Exists (folder))
		{
			DirectoryInfo info = System.IO.Directory.CreateDirectory (folder);
			if (!info.Exists)
				Application.Quit ();
		}

        byte[] bytes = null;
        if (_format == PhotoFormat.PhotoJPG)
            bytes = _lastPhoto.EncodeToJPG();
        else
            bytes = _lastPhoto.EncodeToPNG();

        File.WriteAllBytes(getFilename(idFile), bytes);
        _lastPhotoPath = getFilename(idFile);

        _collection.AddSouvenir(_client.SendPhoto(Path.GetFileName(getFilename(idFile))));
    }

	public void TakePhoto()
	{
        // Take the photo
        _lastPhoto = new Texture2D (_webcamTexture.width, _webcamTexture.height);
        _lastPhoto.SetPixels (_webcamTexture.GetPixels ());
        _lastPhoto.Apply ();

        // Update lastPhotoViwer
        if (_lastPhotoViewer)
        {
            _lastPhotoViewer.enabled = true;
            _lastPhotoViewer.material.mainTexture = _lastPhoto;
            _lastPhotoViewer.SetMaterialDirty();
        }
	}
}
