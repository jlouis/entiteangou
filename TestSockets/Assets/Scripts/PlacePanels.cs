﻿using UnityEngine;
using System.Collections;

public class PlacePanels : MonoBehaviour {

	public Transform[] _outs;

	void Start ()
	{
		RectTransform canvas = GameObject.Find ("Canvas").GetComponent<RectTransform> ();
		foreach (Transform t in _outs)
		{
			Vector3 position = t.position;
			position.x = (canvas.rect.width * 1.5f * canvas.localScale.x);
			t.position = position;
		}


	}
}
