﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MapManager : MonoBehaviour
{

	[Header("Player")]
	public RectTransform _playerPos;
	public double _playerLat;
	public double _playerLon;

	[Header("Entite")]
	public RectTransform _entitePos;
	public double _entiteLat;
	public double _entiteLon;

	[Header("Pos 1")]
	public RectTransform _pos1;
	public double _lat1;
	public double _lon1;

	[Header("Pos 2")]
	public RectTransform _pos2;
	public double _lat2;
	public double _lon2;

	[Header("Rencontre")]
	public GameObject _shareButton = null;
	public float _distanceRencontre;


	[Header("Debug")]
	public Vector2 _relativePos1;
	public Vector2 _relativePos2;

	public bool _lockButton = false;


    RectTransform _rt = null;

	Vector2 getAnchoredPosition(RectTransform rt)
	{
		return new Vector2 (-rt.offsetMax.x, -rt.offsetMax.y);
	}

	void setAnchoredPosition(RectTransform rt, Vector2 pos)
	{
		rt.offsetMax = new Vector2(-rt.offsetMax.x, -rt.offsetMax.y);
	}
		

	void Start ()
	{
        _rt = GetComponent<RectTransform>();

        float imageWidth = _rt.rect.width;
		float imageHeight = _rt.rect.height;
		Debug.Log("canvasWidth = " + imageWidth);
        Debug.Log("canvasHeight = " + imageHeight);

        Vector2 anchoredPos1 = _pos1.anchoredPosition;
		Vector2 anchoredPos2 = _pos2.anchoredPosition;
		// anchoredPos1 = new Vector2 (-_pos1.offsetMax.x, -_pos1.offsetMax.y);
		// anchoredPos2 = new Vector2 (-_pos2.offsetMax.x, -_pos2.offsetMax.y);

		Debug.Log ("anchoredPos1 =" + anchoredPos1);
		Debug.Log ("anchoredPos2 =" + anchoredPos2);

		_relativePos1 = new Vector2 (anchoredPos1.x / imageWidth, anchoredPos1.y / imageHeight); 
		_relativePos2 = new Vector2 (anchoredPos2.x / imageWidth, anchoredPos2.y / imageHeight); 

		Debug.Log ("_relativePos1 = " + _relativePos1);
		Debug.Log ("_relativePos2 = " + _relativePos2);


        /*
		Vector2 anchorMin1 = _pos1.anchorMin;
		Vector2 anchorMax1 = _pos1.anchorMax;

		Debug.Log ("anchorMin1 = " + anchorMin1);
		Debug.Log ("anchorMax1 = " + anchorMax1);

		Debug.Log ("anchorPosition = " + _pos1.anchoredPosition);

		Debug.Log ("Offset = " + _pos1.offsetMax);

		Vector2 anchorMin2 = _pos2.anchorMin;
		Vector2 anchorMax2 = _pos2.anchorMax;

		*/
	}

	Vector2 getRelativePositionFromGPS(double lat, double lon)
	{
		double distForLat = (_relativePos2.y - _relativePos1.y) / (_lat2 - _lat1);
		double distForLon = (_relativePos2.x - _relativePos1.x) / (_lon2 - _lon1);

		Vector2 anchoredPosition = new Vector2();
        anchoredPosition.x = (float)((double)_relativePos1.x + (lon - _lon1) * distForLon);
        anchoredPosition.y = (float)((double)_relativePos1.y + (lat - _lat1) * distForLat);
		
		return anchoredPosition;
	}

	Vector2 getAnchoredPositionFromRelative(Vector2 pos)
	{
		return new Vector2 (pos.x * _rt.rect.width, pos.y * _rt.rect.height);
	}

	void Update ()
	{

		//setAnchoredPosition(_playerPos, getAnchoredPositionFromRelative(getRelativePositionFromGPS (_playerLat, _playerLon)));
		//setAnchoredPosition(_entitePos, getAnchoredPositionFromRelative(getRelativePositionFromGPS (_entiteLat, _entiteLon)));

		_playerPos.anchoredPosition = getAnchoredPositionFromRelative(getRelativePositionFromGPS (_playerLat, _playerLon));
		_entitePos.anchoredPosition = getAnchoredPositionFromRelative(getRelativePositionFromGPS (_entiteLat, _entiteLon));


		Vector2 diff = new Vector2 ((float)_entiteLat - (float)_playerLat, (float)_entiteLon - (float)_playerLon);
		float distance = diff.magnitude;

		if (_lockButton)
		{
			//Debug.Log ("distance = " + distance);
			bool rencontre = distance < _distanceRencontre;
			_shareButton.SetActive (rencontre);
		}
	}
}
