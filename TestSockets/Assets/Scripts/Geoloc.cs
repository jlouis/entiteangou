﻿using UnityEngine;
using System.Collections;

public class Geoloc : MonoBehaviour
{
	public MapManager map = null;
	IEnumerator Start ()
	{
		
		if (!Input.location.isEnabledByUser)
		{
			Debug.LogError ("!Input.location.isEnabledByUser");
			//Application.Quit ();
			yield break;
		}


		Input.location.Start ();
		
		int maxWait = 20;
		while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }
		
		
		// Service didn't initialize in 20 seconds
        if (maxWait < 1)
        {
            print("Timed out");
            yield break;
        }

        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {
            print("Unable to determine device location");
        }
	}
	
	void Update ()
	{
		map._playerLat = Input.location.lastData.latitude;
		map._playerLon = Input.location.lastData.longitude;

    	//print("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);
	}
}
