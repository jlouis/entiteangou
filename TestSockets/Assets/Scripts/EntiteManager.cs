﻿using UnityEngine;
using System.Collections;

public class EntiteManager : MonoBehaviour
{
    public GameObject _entiteButton = null;
	public float _transitionSpeed = 1.0f;

	float _initialAlpha;
	public float _timer = 0;

	bool _visible = true;

    //ClientEntite monClient = null;

    void Start ()
	{
        //SetVisible(false);
		_initialAlpha = GetComponentInChildren<ParticleSystem> ().startColor.a;
        //monClient = new ClientEntite();
        //monClient.ConnectToServeur("192.168.1.12");
    }
	
	void Update ()
	{
		Color col = GetComponentInChildren<ParticleSystem> ().startColor;
		if (_visible && col.a < _initialAlpha)
			col.a += Time.deltaTime * _transitionSpeed;
		else if (!_visible)
			col.a -= Time.deltaTime * _transitionSpeed;

		foreach (ParticleSystem ps in GetComponentsInChildren<ParticleSystem>())
		{
			ps.startColor = col;
		}

	}


    public void SetVisibleSpc(bool visible)
    {
        if (_entiteButton.activeInHierarchy && visible)
            return;

        SetVisible(visible);
    }

	public void SetVisible(bool visible)
	{
		_visible = visible;
	}


}
