﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SouvenirTextPanel : MonoBehaviour
{
    public InputField _text = null;
    public AppClient _client = null;
    public CollectionManager _collection = null;

    public void SendText()
    {
        _collection.AddSouvenir(_client.SendText(_text.text));
    }


}
