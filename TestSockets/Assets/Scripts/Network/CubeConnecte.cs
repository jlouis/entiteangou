﻿using UnityEngine;
using System.Collections;

public class CubeConnecte : MonoBehaviour
{
    public string _ip = "192.168.1.12";

    ClientEntite myClientEntite;
    bool isWaitingResponse;
	// Use this for initialization
	void Start () {
        myClientEntite = new ClientEntite();
        myClientEntite.SetFolders(@"./Assets/Souvenirs/", @"./Assets/PhotosPrises/");
        myClientEntite.ConnectToServeur(_ip);
        Debug.Log("connecté au serveur");
        isWaitingResponse = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.A))
        {
            // si appui sur A on envoi un souvenir
            Debug.Log("Envoi d'un fichier");
            myClientEntite.EchangeDeSouvenir('i',"rien","hd.jpg");
            Debug.Log("Reception Fichier termine");
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            Vector2 coord = myClientEntite.AskCoordinates();
            Debug.Log(coord);
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            bool reponse = myClientEntite.CanIMeetOrthos();
            Debug.Log("puis-je rencontrer Orthos ? "+reponse);
        }
    }

    void OnDestroy()
    {
        myClientEntite.theServer.Close();
    }
}
