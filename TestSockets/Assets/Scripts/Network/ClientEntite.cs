﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Text;
using System.Net.Sockets;
using System.Threading;

public class ClientEntite  {

    public TcpClient theServer;
    public TcpClient serveurCoordonnees;
    NetworkStream networkStream;
    NetworkStream nsServCoord;

    private string RepertoireSouvenir;
    private string RepertoirePhotosPrises;
    private string nameFileSouvenirData = "SouvenirData.txt";
    public List<SouvenirData> vecSouvenirData;

    public enum TypeSouvenir
    {
        TYPE_SOUVENIR_IMAGE = 0,
        TYPE_SOUVENIR_TEXTE = 1
    }

    public struct SouvenirData
    {
        public TypeSouvenir type;
        public string nomDuFichier;
        public int dateTimeInSecond;
        public string phrase;
    }

    private string[] Entetes =
    {
        "01-",
        "02-",
        "03-",
        "04-",
        "05-",
        "06-",
        "07-",
        "08-"
    };

    enum typeMessage
    {
        ASK_COORD_TO_SERVER,//01-
        STRUCT_SOUVENIR,//02-
        COORD_FROM_SERVER,//03-
        CAN_I_MEET_ORTHOS,//04-
        REPONSE_MEETING,//05-
        SEND_SOUVENIR_TO_SERVER,//06-
        SERVER_SENT_SOUVENIR,//07-
        SERVER_SENT_NEW_NAME_SOUVENIR,//08-
    }

    public ClientEntite()
    {
        theServer = new TcpClient();
        serveurCoordonnees = new TcpClient();
        vecSouvenirData = new List<SouvenirData>();
    }

    //Chargement des données sauvegardées
    private void LoadSouvenirsData()
    {
		string[] lines = System.IO.File.ReadAllLines(Application.persistentDataPath + RepertoireSouvenir + nameFileSouvenirData);
        foreach(string s in lines)
        {
            string[] subString = s.Split('-');
            SouvenirData sd;
            if (subString[0] == (TypeSouvenir.TYPE_SOUVENIR_TEXTE).ToString())
            {
                sd.type = TypeSouvenir.TYPE_SOUVENIR_TEXTE;            
            }
            else
            {
                sd.type = TypeSouvenir.TYPE_SOUVENIR_IMAGE;
            }
            sd.nomDuFichier = subString[1];
            sd.dateTimeInSecond = int.Parse(subString[2]);
            sd.phrase = subString[3];
            vecSouvenirData.Add(sd);
        }
    }

    // Enregistrement des données sauvegardées
    public void SaveSouvenirsData()
    {
        string[] lines = new string[vecSouvenirData.Count];
        int i = 0;
        foreach(SouvenirData sd in vecSouvenirData)
        {
            if(sd.type == TypeSouvenir.TYPE_SOUVENIR_TEXTE)
            {
                lines[i] = sd.type + "- -" + sd.dateTimeInSecond + "-" + sd.phrase + "-";
                i++;
            }
            else if(sd.type == TypeSouvenir.TYPE_SOUVENIR_IMAGE)
            {
                lines[i] = sd.type + "-" + sd.nomDuFichier + "-" + sd.dateTimeInSecond + "- -";
                i++;
            }
        }
		System.IO.File.WriteAllLines(Application.persistentDataPath + RepertoireSouvenir+nameFileSouvenirData, lines);

    }

    // ajout un souvenir a la liste et le place correctement
    // par rapport a dataTime
    private void AddSouvenir(SouvenirData sd)
    {
        if(vecSouvenirData.Count <= 0)
        {
            vecSouvenirData.Add(sd);
            return;
        }

        bool place = false;
        int i = 0;
        while(!place && i < vecSouvenirData.Count)
        {
            if(sd.dateTimeInSecond > vecSouvenirData[i].dateTimeInSecond)
            {
                vecSouvenirData.Insert(i, sd);
                place = true;
            }
            i++;
        }
        if (!place) vecSouvenirData.Add(sd);
    }

    // Connexion au serveur
    public void ConnectToServeur(string adresseServeur)
    {
        try
        {
            theServer.Connect(adresseServeur, 42137);
            serveurCoordonnees.Connect(adresseServeur, 42138);
        }
        catch (SocketException e)
        {
            Debug.Log("Unable to connect to server : " + e.Message);
            return;
        }
        networkStream = theServer.GetStream();
        nsServCoord = serveurCoordonnees.GetStream();
    }

    // Action d'envoyer un souvenir et d'en recevoir un
    public string EchangeDeSouvenir(char typeFile, string text =" ", string filename ="")
    {
        if(typeFile != 'i' && typeFile != 't')
        {
            Debug.Log("Echange de souvenir : le type de fichier n'est pas bon");
            return "erreur";
        }
        
        if(typeFile == 'i')
        {
            EnvoiFichier(filename);
        }
        else
        {
            string msg = System.String.Empty;
            msg += Entetes[(int)typeMessage.SEND_SOUVENIR_TO_SERVER];
            msg += 1;
            msg += "-t-";
            msg += text + "-";
            SendShortMessage(msg);
        }

        //attente de retour pour ajouter notre souvenir dans notre base
        string retourMessage = RecvShortMessage();
        string[] subString = retourMessage.Split('-');
        SouvenirData mySD;
        mySD.type = (typeFile == 'i' ? TypeSouvenir.TYPE_SOUVENIR_IMAGE : TypeSouvenir.TYPE_SOUVENIR_TEXTE);
        mySD.nomDuFichier = subString[1];
        mySD.dateTimeInSecond = int.Parse(subString[2]);
        mySD.phrase = text;
        AddSouvenir(mySD);

        if(typeFile == 'i') RenameAndMovePicture(filename,mySD.nomDuFichier);

        //reception du souvenir retour
        retourMessage = System.String.Empty;
        retourMessage = RecvShortMessage();
        subString = retourMessage.Split('-');
        SouvenirData newSD;
        newSD.type = (subString[1]=="i" ? TypeSouvenir.TYPE_SOUVENIR_IMAGE : TypeSouvenir.TYPE_SOUVENIR_TEXTE);
        newSD.nomDuFichier = subString[3];
        newSD.dateTimeInSecond = int.Parse(subString[2]);
		//Debug.Log (subString [2]);

        if(newSD.type == TypeSouvenir.TYPE_SOUVENIR_TEXTE)
        {
            newSD.phrase = subString[4];
            AddSouvenir(newSD);
            return newSD.phrase;
        }
        else
        {
            int tailleFichier = int.Parse(subString[4]);
            ReceptionFichier(tailleFichier,newSD.nomDuFichier);
            newSD.phrase = " ";
            AddSouvenir(newSD);
            return newSD.nomDuFichier;
        }

        
    }

    private void RenameAndMovePicture(string filename,string newFilename)
    {
		string sourcePath = Application.persistentDataPath + RepertoirePhotosPrises;
		string targetPath = Application.persistentDataPath + RepertoireSouvenir;

        string sourceFile = System.IO.Path.Combine(sourcePath, filename);
        string destFile = System.IO.Path.Combine(targetPath, newFilename);

        if (!System.IO.Directory.Exists(targetPath))
        {
            System.IO.Directory.CreateDirectory(targetPath);
            //Debug.Log("le dossier n'existe pas");
        }

        if (!System.IO.File.Exists(destFile))
        {
            System.IO.File.Move(sourceFile, destFile);
        }
       
    }

    public Vector2 AskCoordinates()
    {
        Vector2 OrthosCoord = new Vector2();

        //envoi de l'entete
        string entete = System.String.Empty;
        entete += Entetes[(int)typeMessage.ASK_COORD_TO_SERVER];
        int nbEnvoye;
        byte[] msg = Encoding.ASCII.GetBytes(entete);
        nbEnvoye = serveurCoordonnees.Client.Send(msg, msg.Length, SocketFlags.None);
        
        if (nbEnvoye == msg.Length)
        {
            string incomingMsg;
            if (!nsServCoord.DataAvailable)
                Debug.Log("PAS DE DATA DISPO !");

            byte[] buffer = new byte[1024];
            for (int i = 0; i < 1024; ++i)
                buffer[i] = 0;

            StringBuilder myCompleteMessage = new StringBuilder();
            int numberOfBytesRead = 0;

            Debug.Log("Start recv: ");
            numberOfBytesRead = nsServCoord.Read(buffer, 0, buffer.Length);

            Debug.Log("Recv: " + numberOfBytesRead);
            myCompleteMessage.AppendFormat("{0}", Encoding.ASCII.GetString(buffer, 0, numberOfBytesRead));

            Debug.Log("End recv: ");

            incomingMsg= myCompleteMessage.ToString();

            // decoupe du message
            Char delimiter = '-';
            string[] subString = incomingMsg.Split(delimiter);
            // on verifie qu'on recoit le bon retour
            if (subString[0] == "03"
                && subString.Length >= 3)
            {
                try
                {
                    OrthosCoord.x = float.Parse(subString[1]);
                    OrthosCoord.y = float.Parse(subString[2]);
                }
                catch (Exception e)
                {
                    Debug.Log("erreur parse coordonnee " + e.ToString());
                }
            }
            else
            {

            }
        }
        else
        {
            OrthosCoord.x = 0.0f;
            OrthosCoord.y = 0.0f;
        }
        
        return OrthosCoord;
    }

    public bool CanIMeetOrthos()
    {
        if (EnvoiEntete(typeMessage.CAN_I_MEET_ORTHOS))
        {
            string incomingMsg = RecvShortMessage();
            // decoupe du message
            Char delimiter = '-';
            string[] subString = incomingMsg.Split(delimiter);
            // on verifie qu'on recoit le bon retour
            if (subString[0] == "05"
                && subString.Length >= 2)
            {
                return subString[1] == "y";
            }
            else
            {
                return false;
            }

        }
        else
        {
            return false;
        }
        
    }

    //envoi d'un fichier au serveur
    private void EnvoiFichier(string filename)
    {
        
		FileStream fs = new FileStream(Application.persistentDataPath + RepertoirePhotosPrises + filename, FileMode.Open);
        try
        {
            //envoi de l'entete + taille
            string entete = System.String.Empty;
            entete += Entetes[(int)typeMessage.SEND_SOUVENIR_TO_SERVER];
            entete += fs.Length + "-i-";
            networkStream.Write(Encoding.ASCII.GetBytes(entete), 0, entete.Length);
            networkStream.Flush();
            Thread.Sleep(100);


            int bufferSize = 1024;
            byte[] buffer = null;

            theServer.SendTimeout = 600000;
            theServer.ReceiveTimeout = 600000;

            int bufferCount = Convert.ToInt32(Math.Ceiling((double)fs.Length / (double)bufferSize));

            for (int i = 0; i < bufferCount; i++)
            {
                buffer = new byte[bufferSize];
                int size = fs.Read(buffer, 0, bufferSize);

                theServer.Client.Send(buffer, size, SocketFlags.Partial);
            }
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
        }

        finally{
            fs.Close();
        }
       
    }

    // envoi d'un entete au serveur pour préparer un message
    // envoi vrai si le message a bien été envoyé
    private bool EnvoiEntete(typeMessage _typeMsg, char typeSouvenir='n')
    {
        string entete = System.String.Empty;
        entete += Entetes[(int)_typeMsg];

        switch(_typeMsg)
        {
            case typeMessage.ASK_COORD_TO_SERVER:
                break;
            case typeMessage.STRUCT_SOUVENIR:
                break;
            case typeMessage.COORD_FROM_SERVER:
                break;
            case typeMessage.CAN_I_MEET_ORTHOS:
                String ID = SystemInfo.deviceUniqueIdentifier;
                entete += ID + "-";
                break;
            case typeMessage.REPONSE_MEETING:
                break;
            case typeMessage.SEND_SOUVENIR_TO_SERVER:
                break;
            case typeMessage.SERVER_SENT_SOUVENIR:
                break;
            case typeMessage.SERVER_SENT_NEW_NAME_SOUVENIR:
                break;
            default:
                break;
        }

        return (SendShortMessage(entete));
    }

    //envoi d'un message court
    private bool SendShortMessage(string str)
    {
        int nbEnvoye;
        byte[] msg = Encoding.ASCII.GetBytes(str);
        nbEnvoye = theServer.Client.Send(msg, msg.Length, SocketFlags.None);
        return nbEnvoye == msg.Length;
    }

    private string RecvShortMessage()
    {
		if (!networkStream.DataAvailable)
			Debug.Log ("PAS DE DATA DISPO !");

        byte[] buffer = new byte[1024];
		for (int i = 0; i < 1024; ++i)
			buffer [i] = 0;
        
		//Debug.Log ("En attente :" + networkStream.Length);
		//int nbByteLu = networkStream.Read(buffer, 0, buffer.Length);
        //message = Encoding.ASCII.GetString(buffer, 0, nbByteLu);
		
		StringBuilder myCompleteMessage = new StringBuilder();
          int numberOfBytesRead = 0;
			
		Debug.Log("Start recv: ");
          // Incoming message may be larger than the buffer size.
          //do{
			numberOfBytesRead = networkStream.Read(buffer, 0, buffer.Length);

			Debug.Log("Recv: "+numberOfBytesRead);
			//Debug.Log("Msg: "+buffer);
			myCompleteMessage.AppendFormat("{0}", Encoding.ASCII.GetString(buffer, 0, numberOfBytesRead));
          //}
		//while(networkStream.DataAvailable);
		Debug.Log("End recv: ");
		/*
		while (networkStream.DataAvailable)
		{
			int read = networkStream.Read (buffer, 0, buffer.Length);
			Debug.Log("networkStream NB read = " + read);
			Debug.Log(Encoding.ASCII.GetString(buffer, 0, read));
		}
		*/

        //return message;
		return myCompleteMessage.ToString();
    }

    // Reception d'un fichier du serveur
    private void ReceptionFichier(int tailleFic, string nomFic)
    {
        try
        {
            int bufferSize = 1024;
            byte[] buffer = null;

            theServer.SendTimeout = 600000;
            theServer.ReceiveTimeout = 600000;

            int tailleFichier = tailleFic;
            string nomFichier = nomFic;

			Debug.Log("Reception fichier!! "+tailleFichier); 

            //creation du fichier
			string s = Application.persistentDataPath + RepertoireSouvenir;
            s += nomFichier;
            FileStream fs = File.Create(s, tailleFichier);
            int thisRead = 0;
            buffer = new byte[bufferSize];
			Debug.Log("Before while");   
            while (tailleFichier > 0)
            {
				Debug.Log("Read bout file:");   
                thisRead = networkStream.Read(buffer, 0, bufferSize);
                fs.Write(buffer, 0, thisRead);
                tailleFichier -= thisRead;
				Debug.Log("Reste fichier :"+tailleFichier); 
            }
            fs.Close();

			/*do{
				int nb = networkStream.Read(buffer, 0, buffer.Length);
				Debug.Log("Reste pas lu :"+buffer); 
			}
			while(networkStream.DataAvailable);*/

            Debug.Log("vous avez recu le fichier : " + nomFichier);
        }
        catch(Exception e)
		{
			Debug.Log("EXCEPTION MFK: "+e.ToString()); 
            return;
        }
        

        
    }

    // Receptionniste en charge de trié les messages
    public int Receptionniste(byte[] buffer, int nbRecu)
    {
        string str = System.Text.Encoding.ASCII.GetString(buffer, 0, nbRecu);
        int numRep;
        if (str.Substring(0, 3) == "03-")
        {
            //majCoord
            numRep = 3;

        }
        else if (str.Substring(0, 3) != "07-")
        {
            numRep = 3;
        }
        else
        {
            numRep = 0;
        }

        return numRep;
        //int recv = ns.Read(data, 0, data.Length);
        //    stringData = Encoding.ASCII.GetString(buffer, 0, nbLuDuRead);
        //    Debug.LogLine(stringData);
    }

    public void SetFolders(string repSouvenir="", string repPhotosPrises="")
    {
        RepertoireSouvenir = repSouvenir == "" ? @".\" : repSouvenir;
        RepertoirePhotosPrises = repPhotosPrises == "" ? @".\" : @repPhotosPrises;
    }
}
