﻿using UnityEngine;
using System.Collections;

public class UI_Width : MonoBehaviour
{
	public bool _changePosition = false;
	public bool _changePositionZero = false;

	RectTransform _canvas = null;
	RectTransform _rt = null;

	void Start ()
	{
		_canvas = GameObject.Find ("Canvas").GetComponent<RectTransform> ();
		_rt = GetComponent<RectTransform> ();
	}
	
	void Update ()
	{
		Vector2 size = _rt.sizeDelta;
		size.x = _canvas.rect.width / 2;
		_rt.sizeDelta = size;

		if (_changePosition)
			_rt.anchoredPosition = new Vector2 (-(_canvas.rect.width / 2) + size.x / 2, _rt.anchoredPosition.y);
		if (_changePositionZero)
			_rt.anchoredPosition = new Vector2 (size.x / 2, _rt.anchoredPosition.y);
	}
}
