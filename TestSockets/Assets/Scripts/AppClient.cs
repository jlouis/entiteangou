﻿using UnityEngine;
using System.Collections;
using System.Threading;

public class AppClient : MonoBehaviour
{
    public string _ip = "192.168.1.12";

    public MapManager _map;

    ClientEntite _myClientEntite = null;

    public int _geolocDelay = 500;
    Thread _geolocThread = null;


    void Start ()
    {
        _myClientEntite = new ClientEntite();
        _myClientEntite.SetFolders(@"/Assets/Souvenirs/", @"/Assets/PhotosPrises/");
        _myClientEntite.ConnectToServeur(_ip);

        _geolocThread = new Thread(new ThreadStart(UpdateEntitePosition));
        _geolocThread.Start();
    }

    void OnDestroy()
    {
		if (_geolocThread != null)
        	_geolocThread.Abort();
    }


    public void UpdateEntitePosition()
    {
        while (Thread.CurrentThread.IsAlive)
        {
            Thread.Sleep(_geolocDelay);

            Vector2 entitePos = _myClientEntite.AskCoordinates();
			_map._entiteLat = entitePos.x;
			_map._entiteLon = entitePos.y;
            Debug.Log("entitePos = " + entitePos);
        }
    }

    public string SendPhoto(string photo)
    {
        return _myClientEntite.EchangeDeSouvenir('i', "PHOTO", photo);
    }

    public string SendText(string text)
    {
        return _myClientEntite.EchangeDeSouvenir('t', text);
    }
}
