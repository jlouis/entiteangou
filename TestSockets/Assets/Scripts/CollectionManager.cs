﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class CollectionManager : MonoBehaviour
{
	public GameObject _imagePrefab = null;
	public GameObject _textPrefab = null;
	public float _yOffset = 1.0f;
	public float _spacement = 1.0f;

	public string _folderSouvenirs;

	RectTransform _rect = null;
	List<GameObject> _souvenirs = new List<GameObject>();

	List<GameObject> _toPlace = new List<GameObject>();

	void Awake ()
	{
		_rect = GetComponent<RectTransform> ();

		//AddText ("TEXT1");
		//AddText ("TEXT1");
		//AddText ("TEXT1");
		//AddText ("TEXT1");
		//AddImage ("EntPhoto0.png");
		//AddText ("TEXT1");
	}

	void placeItem(GameObject obj)
	{
		obj.transform.SetParent (transform, false);

		RectTransform rt = obj.GetComponent<RectTransform> ();

		float yPos = _yOffset + rt.rect.height / 2;
		if (GetComponentInChildren<Text>() == null)
			yPos = _yOffset + GetComponentInChildren<Text>().GetComponent<RectTransform>().rect.height / 2;

		foreach(GameObject o in _souvenirs)
		{
			yPos += o.GetComponent<RectTransform> ().rect.height;
			yPos += _spacement;
		}


		rt.anchoredPosition = new Vector2 (rt.anchoredPosition.x, -yPos);

		_souvenirs.Add (obj);


		_rect = GetComponent<RectTransform> ();
		Vector2 scale = _rect.sizeDelta;
		scale.y = yPos + rt.rect.height;
		Debug.Log ("Height = " + scale.y);
		_rect.sizeDelta = scale;
	}

	public void AddText(string text)
	{
		GameObject textObj = Instantiate (_textPrefab);
		textObj.GetComponentInChildren<Text> ().text = text;

		//yield return new WaitForEndOfFrame ();

		Vector2 rect = textObj.GetComponent<RectTransform> ().sizeDelta; 
		rect.y = textObj.GetComponentInChildren<Text> ().gameObject.GetComponent<RectTransform> ().rect.height;
		textObj.GetComponent<RectTransform> ().sizeDelta = rect;
		placeItem (textObj);

		_toPlace.Add(textObj);
	}

	/*
	void LateUpdate()
	{
		foreach (GameObject textObj in _toPlace) {


			placeItem (textObj);
		}
		_toPlace.Clear ();
	}*/

	Texture2D loadImage(string filePath)
	{
		Texture2D tex = null;
		byte[] fileData;

		if (File.Exists(filePath))     {
			fileData = File.ReadAllBytes(filePath);
			tex = new Texture2D(2, 2);
			tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
		}
		return tex;
	}

	public void AddImage(string filename)
	{
		GameObject imageObj = Instantiate (_imagePrefab);

		Texture2D tex = loadImage (Application.persistentDataPath + _folderSouvenirs + "/" + filename);
		Debug.Log ("Load : " + Application.persistentDataPath + _folderSouvenirs + "/" + filename);
		imageObj.GetComponentInChildren<Image> ().sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));


		placeItem (imageObj);
	}

    public void AddSouvenir(string data)
    {
		if (data.Length >= 4 &&
			data[data.Length - 4] == '.' &&
            data[data.Length - 3] == 'j' &&
            data[data.Length - 2] == 'p' &&
			data[data.Length - 1] == 'g')
            AddImage(data);
        else
            AddText(data);
    }
}
