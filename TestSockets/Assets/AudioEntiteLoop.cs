﻿using UnityEngine;
using System.Collections;

public class AudioEntiteLoop : MonoBehaviour
{

	public float _distanceLoop;
	AudioSource _audio = null;
	public MapManager _map = null;

	void Start ()
	{
		_audio = GetComponent<AudioSource> ();
	}
	
	void Update ()
	{
		Vector2 diff = new Vector2 ((float)_map._entiteLat - (float)_map._playerLat, (float)_map._entiteLon - (float)_map._playerLon);
		float distance = diff.magnitude;

		float volume = 0.0f;
		if (distance > _distanceLoop)
			volume = (_distanceLoop - distance) / _distanceLoop;

		volume += 0.1f;
		_audio.volume = volume;
	}
}
